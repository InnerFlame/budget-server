<?php

use yii\db\Migration;

class m170126_055159_initial extends Migration
{
    const TBL_USERS = 'users';
    const TBL_OPERATIONS = 'operations';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TBL_USERS, [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()->notNull(),
            'created_at'  => $this->integer(),
            'updated_at'  => $this->integer(),
        ], $tableOptions);

        $this->createTable(self::TBL_OPERATIONS, [
            'id'          => $this->primaryKey(),
            'user_id'     => $this->integer()->notNull(),
            'name'        => $this->string()->notNull(),
            'type'        => $this->integer()->notNull(),
            'amount'      => $this->double()->notNull(),
            'description' => $this->text(),
            'created_at'  => $this->integer(),
            'updated_at'  => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_' . self::TBL_OPERATIONS . '_user_id', self::TBL_OPERATIONS, 'user_id', self::TBL_USERS, 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_' . self::TBL_OPERATIONS . '_user_id', self::TBL_OPERATIONS);

        $this->dropTable(self::TBL_OPERATIONS);
        $this->dropTable(self::TBL_USERS);
    }
}
