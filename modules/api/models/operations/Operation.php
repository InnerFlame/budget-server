<?php

namespace app\modules\api\models\operations;

use app\modules\api\models\users\User;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "operations".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property integer $type
 * @property double $amount
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 */
class Operation extends \yii\db\ActiveRecord
{
    const TYPE_CREDIT = 1;
    const TYPE_DEBIT = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'type', 'amount'], 'required'],
            [['user_id', 'type', 'created_at', 'updated_at'], 'integer'],
            [['amount'], 'number'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'user_id'     => Yii::t('app', 'User ID'),
            'name'        => Yii::t('app', 'Name'),
            'type'        => Yii::t('app', 'Type'),
            'amount'      => Yii::t('app', 'Amount'),
            'description' => Yii::t('app', 'Description'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return OperationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OperationQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getBalance()
    {
        $debits = self::find()->where(['type' => self::TYPE_DEBIT])->sum('amount');
        $credits = self::find()->where(['type' => self::TYPE_CREDIT])->sum('amount');

        return compact('debits', 'credits');
    }
}
