<?php

namespace app\modules\api\models\operations;

use Yii;

/**
 * This is the model class for table "operations".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property double $amount
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 */
class Debit extends Operation
{
    /**
     * TODO дублирование
     * @param $operation_id
     * @return mixed
     */
    public static function loadIfExist($operation_id)
    {
        $credit = self::findOne($operation_id);

        if (!is_object($credit)) {
            return [['field' => 'id', 'message' => 'Debit not found.']];
        }

        return $credit;
    }
}
