<?php

namespace app\modules\api\controllers;

use app\modules\api\models\operations\Credit;
use app\modules\api\models\operations\Operation;
use app\modules\api\models\users\User;
use yii\db\Expression;
use yii\rest\ActiveController;

/**
 * Default controller for the `api` module
 */
class CreditsController extends ActiveController
{
    public $modelClass = 'app\modules\api\models\operations\Credit';

    public function init(){
        header('Access-Control-Allow-Origin: *');
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions =  parent::actions();

        unset($actions['index'], $actions['create'], $actions['view'], $actions['update'], $actions['delete']);

        return $actions;
    }

    /**
     * @param $user_id
     * @return array
     */
    public function actionIndex($user_id)
    {
        User::loadIfExist($user_id);

        $credit = Credit::find()->where(['type' => Operation::TYPE_CREDIT])->all();

        return $credit;
    }

    /**
     * @param $user_id
     * @return array
     */
    public function actionCreate($user_id)
    {
        User::loadIfExist($user_id);

        $credit = new Credit();
        $credit->user_id = $user_id;
        $credit->type = Operation::TYPE_CREDIT;
        $credit->load(\Yii::$app->request->post(), '');
        $credit->save();

        return Operation::getBalance();
    }

    /**
     * @param $user_id
     * @param $operation_id
     * @return mixed
     */
    public function actionView($user_id, $operation_id)
    {
        User::loadIfExist($user_id);

        return Credit::loadIfExist($operation_id);
    }

    /**
     * @param $user_id
     * @param $operation_id
     * @return mixed
     */
    public function actionUpdate($user_id, $operation_id)
    {
        User::loadIfExist($user_id);

        $credit = Credit::loadIfExist($operation_id);
        $credit->load(\Yii::$app->request->post(), '');
        $credit->save();

        return $credit;
    }

    /**
     * @param $user_id
     * @param $operation_id
     * @return mixed
     */
    public function actionDelete($user_id, $operation_id)
    {
        User::loadIfExist($user_id);

        $credit = Credit::loadIfExist($operation_id);

        return $credit->delete();
    }
}