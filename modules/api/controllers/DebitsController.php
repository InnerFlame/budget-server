<?php

namespace app\modules\api\controllers;

use app\modules\api\models\operations\Debit;
use app\modules\api\models\operations\Operation;
use app\modules\api\models\users\User;
use yii\rest\ActiveController;

/**
 * Default controller for the `api` module
 */
class DebitsController extends ActiveController
{
    public $modelClass = 'app\modules\api\models\operations\Debit';

    public function init(){
        header('Access-Control-Allow-Origin: *');
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions =  parent::actions();

        unset($actions['index'], $actions['create'], $actions['view'], $actions['update'], $actions['delete']);

        return $actions;
    }

    /**
     * @param $user_id
     * @return array
     */
    public function actionIndex($user_id)
    {
        User::loadIfExist($user_id);

        $debit = Debit::find()->where(['type' => Operation::TYPE_DEBIT])->all();

        return $debit;
    }

    /**
     * @param $user_id
     * @return array
     */
    public function actionCreate($user_id)
    {
        User::loadIfExist($user_id);

        $credit = new Debit();
        $credit->user_id = $user_id;
        $credit->type = Operation::TYPE_DEBIT;
        $credit->load(\Yii::$app->request->post(), '');
        $credit->save();

        return Operation::getBalance();
    }

    /**
     * @param $user_id
     * @param $operation_id
     * @return mixed
     */
    public function actionView($user_id, $operation_id)
    {
        User::loadIfExist($user_id);

        return Debit::loadIfExist($operation_id);
    }

    /**
     * @param $user_id
     * @param $operation_id
     * @return mixed
     */
    public function actionUpdate($user_id, $operation_id)
    {
        User::loadIfExist($user_id);

        $credit = Debit::loadIfExist($operation_id);
        $credit->load(\Yii::$app->request->post(), '');
        $credit->save();

        return $credit;
    }

    /**
     * @param $user_id
     * @param $operation_id
     * @return mixed
     */
    public function actionDelete($user_id, $operation_id)
    {
        User::loadIfExist($user_id);

        $credit = Debit::loadIfExist($operation_id);

        return $credit->delete();
    }
}
