// debits - операция с + знаком
// credits - операция с - знаком
// users - юзеры

// REST USERS
// http://budget-server/api/users GET получаем всех юзеров
// http://budget-server/api/users/1 GET получаем нужного юзера
// http://budget-server/api/users POST создаем юзера
// name:name
// http://budget-server/api/users/1 PATCH обновляет данные юзера
// name:name
// http://budget-server/api/users/1 DELETE удаляет юзера

// REST CREDITS
// http://budget-server/api/users/1/credits GET получаем все кредиты по заданному юзеру
// http://budget-server/api/users/1/credits/10 GET получаем заданный кредит должен пренадлежать заданному юзеру
// http://budget-server/api/users/1/credits POST создаем кредит заданному юзеру
// name:1212
// amount:100
// description:description
// http://budget-server/api/users/3/credits/17 PATCH редактируем данные кредита, кредит должен быть у такого юзера
// name:12121111111111
// amount:100
// http://budget-server/api/users/3/credits/171 DELETE удаляем кредит, такой кредит должен быть и пренадлежать юзеру